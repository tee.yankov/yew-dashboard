use stdweb::web::{window, History};
use serde_derive::{Deserialize, Serialize};
use yew::{
    agent::{Agent, Context, AgentLink, Transferable, HandlerId},
    services::{ConsoleService}
};

pub struct Router {
    curent_route: Option<String>,
    history: History,
    console: ConsoleService,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
    PushRoute(String),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Response {
    Changed,
    Unchanged,
}

impl Transferable for Request {}
impl Transferable for Response {}

impl Agent for Router {
    type Reach = Context;
    type Message = ();
    type Input = Request;
    type Output = Response;

    fn create(_: AgentLink<Self>) -> Self {
        Router {
            curent_route: None,
            history: window().history(),
            console: ConsoleService::new(),
        }
    }

    fn update(&mut self, _: Self::Message) {}

    fn handle(&mut self, msg: Self::Input, _: HandlerId) {
        match msg {
            Request::PushRoute(s) => {
                self.history.push_state((), "New Page", Some(&s));
                self.curent_route = Some(s.clone());
                self.console.info(&format!("Internal PushRoute - {}", s));
            },
        }
    }
}
