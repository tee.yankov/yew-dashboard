use serde_derive::Deserialize;
use yew::{html, Html};

#[derive(Debug, Deserialize)]
pub struct Book {
    pub bib_key: String,
    pub thumbnail_url: String,
    pub info_url: String,
}

pub fn render_book(book: &Book) -> Html<crate::RootModel> {
    html! {
        <table>
            <th>
                <td>{"Title"}</td>
            </th>
            <th>
                <td>{"Info"}</td>
            </th>
            <tr>
                <td>
                    <img src=book.thumbnail_url />
                </td>
                <td>
                    <a href=book.info_url.clone()>{book.info_url.clone()}</a>
                </td>
            </tr>
        </table>
    }
}
