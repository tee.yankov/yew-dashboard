use yew::{
    html,
    Html,
    Renderable,
    Component,
    ComponentLink,
    ShouldRender,
    Bridge,
    agent::Bridged,
};
use stdweb::{
    web::{EventListenerHandle},
};

pub enum Msg {
    RouteChange,
    InternalRouteChange,
}

pub struct Router {
    context: Box<dyn Bridge<crate::models::router::Router>>,
    current_path: Option<String>,
    navigation_event_handle: Option<EventListenerHandle>,
}

impl Component for Router {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, mut link: ComponentLink<Self>) -> Self {
        let callback = link.send_back(|_| Msg::RouteChange);
        let context = crate::models::router::Router::bridge(callback);

        Router {
            context,
            current_path: None,
            navigation_event_handle: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::RouteChange => {},
            Msg::InternalRouteChange => {},
        }
        true
    }

    // fn mounted(&mut self) -> ShouldRender {
    //     self.navigation_event_handle = Some(window().add_event_listener());

    //     false
    // }
}

impl Renderable<Router> for Router {
    fn view(&self) -> Html<Self> {
        html! {
            <div>
                {"Router"}
            </div>
        }
    }
}
