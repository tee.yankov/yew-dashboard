#![recursion_limit = "512"]

use failure::Error;
use std::collections::HashMap;
use std::time::Duration;
use yew::services::ConsoleService;
use yew::{
    format::{Json, Nothing},
    html,
    services::{
        fetch::{FetchTask, Request, Response},
        interval::IntervalTask,
        FetchService, IntervalService,
    },
    Callback, Component, ComponentLink, Html, Renderable, ShouldRender,
};

mod components;
use components::book::Book;
mod models;

pub struct RootModel {
    console: ConsoleService,
    value: i64,
    interval: IntervalService,
    link: ComponentLink<RootModel>,
    cb: Callback<()>,
    interval_handle: Option<IntervalTask>,
    fetch_service: FetchService,
    task: Option<FetchTask>,
    books: Option<Vec<Book>>,
}

#[derive(Debug)]
pub enum Msg {
    Increment,
    Decrement,
    Bulk(Vec<Msg>),
    FetchReady(Result<ServerResponse, Error>),
    FetchFailed,
    DeleteInterval,
}

pub type ServerResponse = HashMap<String, Book>;

impl Component for RootModel {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, mut link: ComponentLink<Self>) -> Self {
        RootModel {
            console: ConsoleService::new(),
            cb: link.send_back(|_| Msg::Increment),
            value: 0,
            link,
            interval: IntervalService::new(),
            interval_handle: None,
            fetch_service: FetchService::new(),
            task: None,
            books: None,
        }
    }

    fn mounted(&mut self) -> ShouldRender {
        let interval_handle = self.interval.spawn(Duration::from_secs(1), self.cb.clone());
        self.interval_handle = Some(interval_handle);
        let req = Request::get(
            "https://openlibrary.org/api/books?bibkeys=ISBN:0451526538,9780141439495&format=json",
        )
        .body(Nothing)
        .unwrap();
        let task = self.fetch_service.fetch(
            req,
            self.link
                .send_back(|res: Response<Json<Result<ServerResponse, Error>>>| {
                    let (meta, Json(data)) = res.into_parts();
                    if meta.status.is_success() {
                        Msg::FetchReady(data)
                    } else {
                        Msg::FetchFailed
                    }
                }),
        );
        self.task = Some(task);
        false
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Increment => {
                self.value = self.value + 1;
                self.console.log("plus one");
            }
            Msg::Decrement => {
                self.value = self.value - 1;
                self.console.log("minus one")
            }
            Msg::Bulk(list) => {
                for msg in list {
                    self.update(msg);
                    self.console.log("bulk action");
                }
            }
            Msg::FetchReady(res) => {
                let data = res.ok().expect("failed to unwrap response");
                let books: Vec<Book> = data.into_iter().map(|(_, v)| v).collect();
                self.console.log(&format!("{:?}", books));
                self.books = Some(books);
            }
            Msg::FetchFailed => {
                self.console.error("fetch failed");
            }
            Msg::DeleteInterval => {
                self.interval_handle = None;
            }
        }
        true
    }
}

impl Renderable<RootModel> for RootModel {
    fn view(&self) -> Html<Self> {
        html! {
            <div>
                <nav class="menu">
                    <button onclick=|_| Msg::Increment>{"Increment"}</button>
                    <button onclick=|_| Msg::Decrement>{"Decrement"}</button>
                    <button onclick=|_| Msg::Bulk(vec![Msg::Increment, Msg::Increment])>{"Increment Twice"}</button>
                    <button onclick=|_| Msg::DeleteInterval>{"Delete Interval"}</button>
                </nav>
                <p>{self.value}</p>
                {if let Some(books) = &self.books {
                      html! {
                          <div>
                              {books.into_iter().map(|book| components::book::render_book(&book) ).collect::<Html<Self>>()}
                          </div>
                      }
                  } else {
                      html! { <p>{"Loading..."}</p> }
                  }
                }
            </div>
        }
    }
}
